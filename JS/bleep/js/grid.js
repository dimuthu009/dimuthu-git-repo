$(document).ready(function () {

        function success() {
            $.ajax({
                url: 'http://demo.w-bo.com/api/pos/sample_data',
                type: 'GET',
                crossOrigin: true,
                success: function(data) {
                    //called when successful
                    //console.log( "Request received: " + data );
                    jsonData = $.parseJSON(data);
                    koRenderer(jsonData);
                },
                error: function(e) {
                    //called when there is an error
                    console.log('erroro'+e.message);
                }
            });
        }

        function koRenderer(jsonData){
            var self = this;
            var masterModel = {};
            self.alldata = ko.observableArray();
            self.alldata.removeAll();
            $.each(jsonData, function (index, jsonData) {
                self.alldata.push(jsonData);
            });

            self.alldata.sort(function (left, right) {
                return moment.utc(left.date).diff(moment.utc(right.date))
            });

            //This basically means showing all dates in observable
            self.startDate = ko.observable('2016-12-01');
            self.endDate = ko.observable('2016-12-31');


            self.NewdateFilter = ko.observable();

            self.filterDate = ko.computed(function (data) {
                var value1 = self.startDate();
                var value2 = self.endDate();
                return ko.utils.arrayFilter(self.alldata(), function (item) {
                    return value1 < item.date && item.date < value2;
                });


            });

            masterModel.model = ko.mapping.fromJS(self.filterDate);
            ko.applyBindings(masterModel);
        }
		
        success();
    });