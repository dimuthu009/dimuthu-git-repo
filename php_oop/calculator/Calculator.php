<?php

class Calculator{

protected $result;
protected $operation;

	public function setOperation(OperatorInterface $operation){

		$this->operation=$operation;
		//var_dump($operation);

	}

	public function Calculate (){

		//print_r(func_get_args());

		foreach (func_get_args() as $number) {

			# code...
			$this->result = $this->operation->run($number, $this->result);


		}
	}

	public function getResult(){

		return $this->result;

	}

}
