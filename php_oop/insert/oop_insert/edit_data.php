<?php
include_once 'dbMySql.php';
$con = new DB_con();
$user_id = $_GET['userid'];

$res = $con->select_user($user_id);
 while($row = mysql_fetch_row($res)){
  $fname = $row[1];
  $lname = $row[2]; 
  $city = $row[3];
            
 }
 
// data insert code starts here.
if(isset($_POST['btn-edit'])){
 $fname = $_POST['first_name'];
 $lname = $_POST['last_name'];
 $city = $_POST['city_name'];
 
 $res = $con->update($user_id,$fname,$lname,$city);
 if($res){
?>

  <script>
  	alert('Record updated...');
  	window.location='index.php'
  </script>
  
  <?php
 }
 else
 {
  ?>
  <script>
  alert('error inserting record...');
        window.location='index.php'
        </script>
  <?php
 }
}
// data insert code ends here.

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP Data Update and Select Data Using OOP - By Cleartuts</title>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
<center>

<div id="header">
 <div id="content">
    <label>PHP Data Update and Select Data Using OOP - By Cleartuts</label>
    </div>
</div>
<div id="body">
 <div id="content">
    <form method="post">
    <table align="center">
    <tr>
    <td><input type="text" name="first_name" placeholder="First Name" value="<?=$fname;?>" required /></td>
    </tr>
    <tr>
    <td><input type="text" name="last_name" placeholder="Last Name" value="<?=$lname;?>" required /></td>
    </tr>
    <tr>
    <td><input type="text" name="city_name" placeholder="City" value="<?=$city;?>" required /></td>
    </tr>
    <tr>
    <td>
    <button type="submit" name="btn-edit"><strong>edit</strong></button></td>
    </tr>
    </table>
    </form>
    </div>
</div>

</center>
</body>
</html>