<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class pagesController extends Controller
{

public function getIndex(){
    return view('welcome');
}

public function getAbout(){
    $compName = "Softreact";
    $userRegistered = true;

    $users = array("dimuthu", "tharindu", "michelle";);
    return view('pages.about') 
    -> with ("companyName", $compName)
    -> with ("isUserRegistered", $userRegistered);
}
   
public function getContacts(){
    return view('pages.contact');
}    

public function getHelp(){
    return view('pages.help');
}

}
