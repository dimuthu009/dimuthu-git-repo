<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => '/', 'uses' => 'pagesController@getIndex']);
Route::get('about', ['as' => 'about', 'uses' => 'pagesController@getAbout']);
Route::get('contact', ['as' => 'contact', 'uses' => 'pagesController@getContacts']);
Route::get('help', ['as' => 'help', 'uses' => 'pagesController@getHelp']);

Route::get('first/second', function () {
    return view('pages.test');
});