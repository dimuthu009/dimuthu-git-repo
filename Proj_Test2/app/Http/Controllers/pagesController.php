<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class pagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('welcome');
    }

     public function getAbout()
    {
        $first ="Dimuthu";
        $last = "Fernando";

        $full =  $first . " " . $last;
        $email = "dimuthu009@gmail.com";

        $data = [];
        $data['fullname']=$full;
        $data['email']=$email;


        return view('pages.about')
        -> withData($data);
    }

     public function getContact()
    {
        return view('pages.contact');
    }


}
